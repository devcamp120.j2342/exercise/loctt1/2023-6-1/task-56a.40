package com.devcamp.s10.task56a40.restapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestapiApplication.class, args);
		Circle circle = new Circle(2);
		System.out.println("Circle");
		System.out.println(circle.getArea());
		System.out.println(circle.getCirumference());
		System.out.println(circle.toString());
	}

}
