package com.devcamp.s10.task56a40.restapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class CircleController {
     @GetMapping("/circle-area")
     public double getCircleArea(@RequestParam("radius") double radius){
          Circle circle = new Circle(radius);
          System.out.println(circle.toString());
          return circle.getArea();
     }
     public double getCircleCirumference(@RequestParam("radius") double radius){
          Circle circle = new Circle(radius);
          System.out.println(circle.toString());
          return circle.getCirumference();
     }
}
