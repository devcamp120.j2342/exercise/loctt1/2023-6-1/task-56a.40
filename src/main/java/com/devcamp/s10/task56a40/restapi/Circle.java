package com.devcamp.s10.task56a40.restapi;

public class Circle {
     private double radius = 1.0;

     public Circle() {
     }

     public Circle(double radius) {
          this.radius = radius;
     }

     public double getRadius() {
          return radius;
     }

     public void setRadius(double radius) {
          this.radius = radius;
     }

     public double getArea(){
          return 2 * Math.PI * radius;
     }

     public double getCirumference(){
          return Math.PI * radius * radius;
     }

     @Override
     public String toString() {
          return "Circle [radius=" + this.radius + "]";
     }

     
}
